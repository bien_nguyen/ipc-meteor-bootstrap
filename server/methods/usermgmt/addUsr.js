// Created by Sean Vo 5.20.2016

Meteor.methods({
	// addUsr arg obj = {username:,pwd:,grp:}
	addUsr : function (obj) { 
		var userInfo = {};
		var roles = [];
		var id;

		// Check permission to add users
		if (!(hasRole("setusr", Meteor.user().username)))
		{
			throw new Meteor.Error("permission-denied", "User is not allowed to create users");
			return 0;
		}

		// Verify object is passed
		if (typeof obj != 'object' || obj == null)
		{
			throw new Meteor.Error("incorrect-arg", "Incorrect argument type.");
			return 0;
		}

		// Check required data
		if (!('username' in obj) || !('pwd' in obj) || !('grp' in obj) || !('fname' in obj) || !('lname' in obj))
		{
			throw new Meteor.Error("incorrect-property", "Object is missing a property");
			return 0;
		}

		// Check if the username already exists
		if (Accounts.findUserByUsername(obj.username))
		{
			throw new Meteor.Error("user-exist", "Username already exists");
			return 0;
		}

		/*
		// Check if fname && lname && mi exists
		if (Meteor.users.findOne({"fname" : {$regex : new RegExp(obj.fname, "i")} , "lname" : {$regex : new RegExp(obj.lname, "i")} , "mi" : {$regex : new RegExp(obj.mi,"i")} }))
		{
			console.log("lname " + obj.lname);
			console.log(Meteor.users.findOne({"fname" : {$regex : new RegExp(obj.fname, "i")} , "lname" : {$regex : new RegExp(obj.lname, "i")} , "mi" : {$regex : new RegExp(obj.mi,"i")} }));
			console.log("Thrown here");
			throw new Meteor.Error("duplicate-name", obj.fname + " " + obj.mi + " " + obj.lname + " " + "already has an account");
			return 0;
		}
		*/

		// Validate data

		// Username
		if (isAlphaNumeric(obj.username))
		{
			userInfo.username = obj.username;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Please enter only letters or numbers for username");
			return 0;
		}

		// password
		if (obj.pwd.length >= 8 && obj.pwd.length <= 15)
		{
			if (validatePassword(obj.pwd))
			{
				userInfo.pwd = obj.pwd;
			}
			else
			{
				throw new Meteor.Error("invalid-password", "Invalid Password. Only alphanumeric and '$' '@' '!' are allowed");
				return 0;
			}
		}
		else
		{
			throw new Meteor.Error("invalid-password", "Password is too short or long. Requirement is 8 to 15 characters");
			return 0;
		}

		// First name must be entered
		if (obj.fname.length == 0)
		{
			throw new Meteor.Error("invalid-value", "A first name must be entered");
		}

		// Last name must be entered
		if (obj.lname.length == 0)
		{
			throw new Meteor.Error("invalid-value", "A last name must be entered");
		}

		// Validate the rest of the entered data
		if (validateAndSetUsrObj(obj, userInfo) == 0)
		{
			return 0;
		}

		// Add the extra user information for server use
		userInfo.stat = "INACTIVE";
		userInfo.pwdate = "";
		userInfo.pwhist = [
				 		{
							"pw" : "",
							"t" : 0
						},
				 		{
							"pw" : "",
							"t" : 0
						},
				 		{
							"pw" : "",
							"t" : 0
						},
				 		{
							"pw" : "",
							"t" : 0
						},
				 		{
							"pw" : "",
							"t" : 0
						}
					];
		userInfo.exp = 0;
		userInfo.pwcnt = 0;
		userInfo.login = 0;
		userInfo.lastLogin = "";
		userInfo.ses = [0,0,0];

		// grp
		if (validateGrp(obj.grp))
		{
			userInfo.grp = obj.grp;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Invalid group");
			return 0;
		}
		// end Validate data

		// Create user account
		id = Accounts.createUser({
			username: userInfo.username,
			password: userInfo.pwd,
		});

		if (!(Accounts.findUserByUsername(userInfo.username)))
		{
			throw new Meteor.Error("create-error" , "Could not create user");
			return 0;
		}

		Meteor.users.update(id, { 
			$set : userInfo
		});

		GRPS.update({_id : userInfo.grp},
		{$addToSet : {users : userInfo.username}}
		);

		// Add roles and groups to newly add user
		//if (roles.length > 0) {
			//Roles.addUsersToRoles(id, roles, userInfo.grp);
		//}

		return 1;
	}
	// end addUsr

})

validateAndSetUsrObj = function (inObj, outObj)
{
	// Last Name
	if (inObj.lname && !(inObj.lname.match(/\s+/)) && inObj.lname != "")
	{
		if (validateName(inObj.lname))
		{
			outObj.lname = inObj.lname;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Invalid Last Name");
			return 0;
		}
	}
	else
	{
		//outObj.lname = "";
		throw new Meteor.Error("missing-field", "A last name must be entered");
	}

	// First Name
	if (inObj.fname && !(inObj.fname.match(/\s+/)) && inObj.fname != "")
	{
		if (validateName(inObj.fname))
		{
			outObj.fname = inObj.fname;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Invalid First Name");
			return 0;
		}
	}
	else
	{
		//outObj.fname = "";
		throw new Meteor.Error("missing-field", "A first name must be entered");
	}

	// Middle Initial
	if (inObj.mi)
	{
		if (validateMI(inObj.mi))
		{
			outObj.mi = inObj.mi;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Please enter one letter for MI");
			return 0;
		}
	}
	else
	{
		outObj.mi = "";
	}

	// EID
	if (inObj.eid)
	{
		if (isNumeric(inObj.eid))
		{
			outObj.eid = inObj.eid;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Please enter only numbers for EID");
			return 0;
		}
	}
	else
	{
		outObj.eid = "";
	}

	// Address
	if (inObj.addr)
	{
		if (validateAddress(inObj.addr))
		{
			outObj.addr = inObj.addr;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Invalid address");
			return 0;
		}
	}
	else
	{
		outObj.addr = "";
	}

	// City
	if (inObj.city)
	{
		if (validateAddress(inObj.city))
		{
			outObj.city = inObj.city;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Invalid city");
			return 0;
		}
	}
	else
	{
		outObj.city = "";
	}

	// State
	if (inObj.state)
	{
		if (isAlpha(inObj.state))
		{
			outObj.state = inObj.state;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Invalid state");
			return 0;
		}
	}
	else
	{
		outObj.state = "";
	}

	// Zip
	if (inObj.zip)
	{
		if (isNumeric(inObj.zip))
		{
			outObj.zip = inObj.zip;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Please enter only numeric characters for Zip");
			return 0;
		}
	}
	else
	{
		outObj.zip = "";
	}

	// Title
	if (inObj.title)
	{
		if (isAlphaNumeric(inObj.title))
		{
			outObj.title = inObj.title;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Please enter only alphanumeric characters only for Title");
			return 0;
		}
	}
	else
	{
		outObj.title = "";
	}

	// Telephone
	if (inObj.tel)
	{
		if (isNumeric(inObj.tel))
		{
			outObj.tel = inObj.tel;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Please enter only numeric characters only for Telephone");
			return 0;
		}
	}
	else
	{
		outObj.tel = "";
	}

	// E-Mail
	if (inObj.email)
	{
		outObj.email = inObj.email;
	}
	else
	{
		outObj.email = "";
	}

	if (inObj.comment)
	{
			outObj.comment = inObj.comment;
	}
	else
	{
		outObj.comment = "";
	}

	// Supervisor
	if (inObj.supv)
	{
		if (validateName(inObj.supv))
		{
			outObj.supv = inObj.supv;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Invalid Supervisor value");
			return 0;
		}
	}
	else
	{
		outObj.supv = "";
	}

	return 1;
}
// End validateAndSetUsrObj
