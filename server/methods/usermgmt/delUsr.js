// Created by Sean Vo 5.20.2016
Meteor.methods({
	delUsr : function (userid) {

		// Check permission
		if (!(hasRole("setusr", Meteor.user().username)))
		{
			throw new Meteor.Error("permission-denied", "User deletion not allowed");
			return 0;
		}

		// Prevent deletion of user 'admin'
		if (userid == Accounts.findUserByUsername("admin")._id)
		{
			throw new Meteor.Error("deletion-denied", "Cannot delete username \'admin\'");
			return 0;
		}
		// Delete the user
		return Meteor.users.remove( { "_id" : userid } );

	}
	// end delUsr
	
})
