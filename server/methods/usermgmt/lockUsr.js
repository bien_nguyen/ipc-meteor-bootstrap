// lockUsr.js
// Created by Sean Vo 5.6.2016

Meteor.methods({

	lockUsr : function (userid)
	{
		// Check permission
		if (!(hasRole("setusr", Meteor.user().username)))
		{
			throw new Meteor.Error("permission-denied", "Lock user not allowed");
			return 0;
		}

		// Check if user is already locked
		if (Meteor.users.findOne({ "_id" : userid }).stat == "LCK")
		{
			throw new Meteor.Error("status-lock", "The user is already locked");
			return 0;
		}

		// Lock the user
		return Meteor.users.update({ "_id" : userid},{ $set :{ stat : "LCK"}});
	},
	// end lockUsr


})
