// Created by Sean Vo 5.20.2016

Meteor.methods({
	updUsr : function (userid, obj) {
		var userInfo = {};

		// Check permission
		if (userid != Meteor.user())
		{
			if (!(hasRole("setusr", Meteor.user().username)))
			{
				throw new Meteor.Error("permission-denied", "Not allowed to change other user's info");
				return 0;
			}
		}

		// Validation
		if (!obj)
		{
			throw new Meteor.Error("undefined-arg", "Obj argument is undefined/null");
			return 0;
		}

		if ((userInfo = validateUsrUpdObj(obj)) == 0)
		{
			return 0;
		}
		//console.log(userInfo);

		// Update the user

		if (obj.grp)
		{
			//console.log("Updating user group");
			currentGroup = Meteor.users.findOne({_id:userid}).grp
			if (obj.grp != currentGroup)
			{
				if (updateUserGroup(Meteor.users.findOne({_id:userid}).username, currentGroup, obj.grp) == 0)
				{
					console.log("updUsr - updateUserGroup : Error occurred");
					throw new Meteor.Error("error-update-group", "Could not update t_grps");
					return 0;
				}
			}
		}

		//console.log("Updating Users");
		return (Meteor.users.update(userid, {
			$set : userInfo
		}));

	}
	// end updUsr

})

updateUserGroup = function (username, oldGroup, newGroup)
{

	// Add user to the new group
	GRPS.update( { _id : newGroup }, 
		{$push : {users : username}}
	);

	// Remove user from the old group
	GRPS.update( { _id : oldGroup }, 
		{$pull : {users : username}}
	);

	return 1;
}


validateUsrUpdObj = function (userObj)
{
	var retUsrObj = {};
	// Last Name
	if (userObj.lname && !(userObj.lname.match(/\s+/)) && userObj.lname != "")
	{
		if (validateName(userObj.lname))
		{
			retUsrObj.lname = userObj.lname;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Invalid Last Name");
			return 0;
		}
	}

	// First Name
	if (userObj.fname && !(userObj.fname.match(/\s+/)) && userObj.fname != "")
	{
		if (validateName(userObj.fname))
		{
			retUsrObj.fname = userObj.fname;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Invalid First Name");
			return 0;
		}
	}

	// Middle Initial
	if (userObj.mi)
	{
		if (validateMI(userObj.mi))
		{
			retUsrObj.mi = userObj.mi;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Please enter one letter for MI");
			return 0;
		}
	}

	// EID
	if (userObj.eid)
	{
		if (isNumeric(userObj.eid))
		{
			retUsrObj.eid = userObj.eid;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Please enter only numbers for EID");
			return 0;
		}
	}

	// Address
	if (userObj.addr)
	{
		if (validateAddress(userObj.addr))
		{
			retUsrObj.addr = userObj.addr;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Invalid address");
			return 0;
		}
	}

	// City
	if (userObj.city)
	{
		if (validateAddress(userObj.city))
		{
			retUsrObj.city = userObj.city;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Invalid city");
			return 0;
		}
	}

	// State
	if (userObj.state)
	{
		if (isAlpha(userObj.state))
		{
			retUsrObj.state = userObj.state;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Invalid state");
			return 0;
		}
	}

	// Zip
	if (userObj.zip)
	{
		if (isNumeric(userObj.zip))
		{
			retUsrObj.zip = userObj.zip;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Please enter only numeric characters for Zip");
			return 0;
		}
	}

	// Title
	if (userObj.title)
	{
		if (isAlphaNumeric(userObj.title))
		{
			retUsrObj.title = userObj.title;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Please enter only alphanumeric characters only for Title");
			return 0;
		}
	}

	// Telephone
	if (userObj.tel)
	{
		if (isNumeric(userObj.tel))
		{
			retUsrObj.tel = userObj.tel;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Please enter only numeric characters only for Telephone");
			return 0;
		}
	}

	// E-Mail
	if (userObj.email)
	{
		retUsrObj.email = userObj.email;
	}

	if (userObj.comment)
	{
			retUsrObj.comment = userObj.comment;
	}

	// Supervisor
	if (userObj.supv)
	{
		if (validateName(userObj.supv))
		{
			retUsrObj.supv = userObj.supv;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Invalid Supervisor value");
			return 0;
		}
	}

	// grp
	if (userObj.grp)
	{
		if (validateGrp(userObj.grp))
		{
			retUsrObj.grp = userObj.grp;
		}
		else
		{
			throw new Meteor.Error("invalid-value", "Invalid group");
			return 0;
		}
	}

	return retUsrObj;
}
