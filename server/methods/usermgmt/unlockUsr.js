// unlockUsr.js
// Created by Sean Vo 5.6.2016

Meteor.methods({

	unlockUsr : function (userid)
	{
		// Check permission
		if (!(hasRole("setusr", Meteor.user().username)))
		{
			throw new Meteor.Error("permission-denied", "Unlock user not allowed");
			return 0;
		}

		// Check if user is locked
		if (Meteor.users.findOne({ "_id" : userid }).stat != "LCK")
		{
			throw new Meteor.Error("status-lock", "The user is not locked");
			return 0;
		}

		// "Unlock" the user
		return Meteor.users.update({ "_id" : userid},{ $set :{ stat : "INACTIVE"}});
	}
	// end unlockUsr

})
