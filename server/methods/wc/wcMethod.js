// Created by Sean Vo 4.20.2016
// Changelog
//	Sean 5.17.2016 - Modified user credential code for the updated t_grp, t_roles mongodb
Meteor.methods({
	updWc : function (obj) {
		var selector = { "_id" : 0};
		var modifier = obj;
		var user = Meteor.user();

		// Check the user credentials
		if (!(hasRole("setwc", Meteor.user().username)))
		{
			throw new Meteor.Error("access-denied", "User does not have permission to update the Wire Center");
			return 0;
		}
		// validate the data received
		if (typeof modifier != 'object' || obj == null)
		{
			throw new Meteor.Error("incorrect-arg", "Incorrect argument type.");
			return 0;
		}

		// update the database
		WC.update(selector,modifier);
		IPCDB.update(selector,modifier,{upsert:true});
		return 1;
	}
})

