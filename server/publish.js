Meteor.publish('system', function() {
	return IPCDB.find({_id:"system"}, {
		fields : {
			"pwr_front": 1,
			"pwr_rear": 1,
			"license": 1,
			"time":1,
			"frameid":1,
			"alarm":1,
			"release":1,
			"username":1
		}
	});
});

Meteor.publish('t_wc', function() {
	return WC.find();
});

Meteor.publish('t_ports', function() {
	return PORTS.find();
});

Meteor.publish('t_roles', function() {
	return ROLES.find();
});

Meteor.publish('t_grps', function() {
	return GRPS.find();
});

Meteor.publish('user-details', function() {
	return Meteor.users.find({},
		{
			fields : {
				"username": 1,
				"lname" : 1,
				"fname" : 1,
				"mi" : 1,
				"ssn" : 1,
				"addr" : 1,
				"city" : 1,
				"state" : 1,
				"zip" : 1,
				"title" : 1,
				"tel" : 1,
				"email" : 1,
				"com" : 1,
				"supv" : 1,
				"stat" : 1,
				"grp" : 1
			}
		}
	);
});

Meteor.publish('userData', function() {
	return Meteor.users.find({});
});
