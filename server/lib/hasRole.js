// Created by Sean Vo 5.17.2016
hasRole = function ( rolename, username )
{
	var groups = [];

	// Check argument type
	if (typeof rolename == 'string' && typeof username == 'string')
	{
		// set groups to contain array of grps
		groups = ROLES.findOne( { _id : rolename } ).grps;

		// Check if the user is part of the listed groups
		if (GRPS.findOne( { users : username, _id : { $in : groups } } ))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		console.log("hasRole() : incorrect argument passed");
		return false;
	}
}
