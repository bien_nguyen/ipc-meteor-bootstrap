angular.module('rt600').config(function ($urlRouterProvider, $stateProvider, $locationProvider) {
	//$locationProvider.html5Mode(true);

	$stateProvider
		.state('warning', {
			url:'/',
			template: '<warning></warning>'
		})
		.state('mainpage', {
			template: '<main-page></main-page>'
		})
		.state('mio1', {
			template: '<mio1></mio1>'
		})
		.state('ipc', {
			template: '<ipc></ipc>'
		});

	$urlRouterProvider.otherwise("/");
});

angular.module('rt600').run(function($rootScope, $state) {
	$rootScope.$watch(
		function () {
			var userId = $rootScope.currentUser;
			return userId;
		},
		function (newValue, oldValue) {
			if (!newValue) {
				$state.go('warning');
			}
			else if ($state.is('warning') && newValue) {
				$state.go('mainpage');
			}
		}
	);
});
