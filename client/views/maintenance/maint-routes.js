angular.module('rt600').config(function ($stateProvider) {

	$stateProvider
		.state('maint-conn', {
			template: '<maint-conn></maint-conn>'
		})
		.state('lck-unlck-card', {
			template: '<lckunlck-card></lckunlck-card>'
		})
		.state('lck-unlck-node', {
			template: '<lckunlck-node></lckunlck-node>'
		})
		.state('alm-admin', {
			template: '<alm-admin></alm-admin>'
		})
		.state('path-admin', {
			template: '<path-admin></path-admin>'
		});
});
