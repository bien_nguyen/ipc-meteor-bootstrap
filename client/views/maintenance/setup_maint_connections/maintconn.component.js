angular.module('rt600').directive('maintConn', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/maintenance/setup_maint_connections/maintconn.html',
		controllerAs: 'maintConn',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
