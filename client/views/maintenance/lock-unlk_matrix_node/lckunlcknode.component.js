angular.module('rt600').directive('lckunlckNode', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/maintenance/lock-unlk_matrix_node/lckunlcknode.html',
		controllerAs: 'lckunlckNode',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
