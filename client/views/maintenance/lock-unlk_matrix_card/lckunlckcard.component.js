angular.module('rt600').directive('lckunlckCard', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/maintenance/lock-unlk_matrix_card/lckunlckcard.html',
		controllerAs: 'lckunlckCard',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
