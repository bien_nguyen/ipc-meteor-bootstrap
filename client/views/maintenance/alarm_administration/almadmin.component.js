angular.module('rt600').directive('almAdmin', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/maintenance/alarm_administration/almadmin.html',
		controllerAs: 'almAdmin',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
