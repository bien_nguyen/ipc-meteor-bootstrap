angular.module('rt600').directive('pathAdmin', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/maintenance/path_administration/pathadmin.html',
		controllerAs: 'pathAdmin',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
