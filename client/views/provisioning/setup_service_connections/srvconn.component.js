angular.module('rt600').directive('srvConn', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/provisioning/setup_service_connections/srvconn.html',
		controllerAs: 'srvConn',
		controller: function($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
