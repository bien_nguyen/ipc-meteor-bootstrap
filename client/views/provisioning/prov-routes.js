angular.module('rt600').config(function ($stateProvider) {

	$stateProvider
		.state('srv-conn', {
			template: '<srv-conn></srv-conn>'
		})
		.state('batch-admin', {
			template: '<batch-admin></batch-admin>'
		});
});
