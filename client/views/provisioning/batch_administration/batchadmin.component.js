angular.module('rt600').directive('batchAdmin', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/provisioning/batch_administration/batchadmin.html',
		controllerAs: 'batchAdmin',
		controller: function($scope, $reactive) {
			$reactive(this).attach($scope);

			$scope.loadFile = () => {
  				var batchFile = document.getElementById('batchFile');
				
				if (batchFile.files.length === 0) {
					return;
				}

				var reader = new FileReader();
				reader.onload = function(progressEvent){
				    // Entire file
					console.log(this.result);

				    // By lines
				    var lines = this.result.split('\n');
				    var tbl = $("#tblBatch>tbody");
				    tbl.empty();

				    for(var line = 0; line < lines.length; line++){
				    	tbl.append("<tr><td>" + line + "</td><td>" + lines[line] + "</td></tr>");
				    }
				};
				
				reader.readAsText(batchFile.files[0]);
			}
		}
	}
});
