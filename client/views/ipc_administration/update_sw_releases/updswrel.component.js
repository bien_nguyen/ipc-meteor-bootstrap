angular.module('rt600').directive('updswRel', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/ipc_administration/update_sw_releases/updswrel.html',
		controllerAs: 'updswRel',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
