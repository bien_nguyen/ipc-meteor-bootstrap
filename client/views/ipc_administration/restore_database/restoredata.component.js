angular.module('rt600').directive('restoreData', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/ipc_administration/restore_database/restoredata.html',
		controllerAs: 'restoreData',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
