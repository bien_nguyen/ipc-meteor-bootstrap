angular.module('rt600').directive('ipcrefData', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/ipc_administration/ipc_reference_data/ipcrefdata.html',
		controllerAs: 'ipcrefData',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
