angular.module('rt600').config(function ($stateProvider) {

	$stateProvider
		.state('backup-data', {
			template: '<bkup-data></bkup-data>'
		})
		.state('restore-data', {
			template: '<restore-data></restore-data>'
		})
		.state('upd-sw-rel', {
			template: '<updsw-rel></updsw-rel>'
		})
		.state('system-shutdown', {
			template: '<sys-shutdown></sys-shutdown>'
		})
		.state('ipc-ref-data', {
			template: '<ipcref-data></ipcref-data>'
		})
		.state('event-log', {
			template: '<event-log></event-log>'
		});
});
