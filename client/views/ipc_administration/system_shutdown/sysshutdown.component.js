angular.module('rt600').directive('sysShutdown', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/ipc_administration/system_shutdown/sysshutdown.html',
		controllerAs: 'sysShutdown',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
