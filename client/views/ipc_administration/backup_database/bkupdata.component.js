angular.module('rt600').directive('bkupData', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/ipc_administration/backup_database/bkupdata.html',
		controllerAs: 'bkupData',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
