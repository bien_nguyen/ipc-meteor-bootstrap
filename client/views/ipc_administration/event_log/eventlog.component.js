angular.module('rt600').directive('eventLog', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/ipc_administration/event_log/eventlog.html',
		controllerAs: 'eventLog',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
