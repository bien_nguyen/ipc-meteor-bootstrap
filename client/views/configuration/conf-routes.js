angular.module('rt600').config(function ($stateProvider) {

	$stateProvider
		.state('wire-center', {
			template: '<wire-center></wire-center>'
		})
		.state('port-map', {
			template: '<port-map></port-map>'
		});
});
