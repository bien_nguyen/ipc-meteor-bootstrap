angular.module('rt600').directive('portMap', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/configuration/port_mapping/portmap.html',
		controllerAs: 'portMap',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);

			console.log(PORTS.find());
			console.log(ALMS.find());

			this.helpers({
				portmaps: () => {
					console.log(PORTS.findOne());
					return PORTS.find({});
				}
			});
		}
	}
});
