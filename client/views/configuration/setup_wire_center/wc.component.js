angular.module('rt600').directive('wireCenter', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/configuration/setup_wire_center/wc.html',
		controllerAs: 'wireCenter',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);

			this.subscribe('t_wc');

			this.wcobj = {};

			this.test = "aaa";

			this.updWc = () => {
				this.call('updWc', this.wcobj, (err, result) => {});
			};

			this.helpers({
				wc: () => {
					this.wcobj = IPCDB.findOne({_id:"t_wc"});
					return this.wcobj;
				},
				checkRole: () => {
					console.log(Roles.userIsInRole(Meteor.userId(), 'admin'));
					return (Roles.userIsInRole(Meteor.userId(), 'admin'));
				}
			});
		},
		link: function ($scope, element, attrs) {
            /*
            $("#tblWc").dataTable({  
        		"scrollY": 200,
        		"scrollX": true 
            });
			*/
        }
	}
});
