angular.module('rt600').directive('maintLog', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/reports/maintenance_log/maintlog.html',
		controllerAs: 'maintLog',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
