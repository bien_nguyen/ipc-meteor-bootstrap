angular.module('rt600').directive('wcconfLog', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/reports/wc-configuration_log/wcconflog.html',
		controllerAs: 'wcconfLog',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
