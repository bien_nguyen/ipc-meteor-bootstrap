angular.module('rt600').directive('almLog', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/reports/alarm_log/almlog.html',
		controllerAs: 'almLog',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
