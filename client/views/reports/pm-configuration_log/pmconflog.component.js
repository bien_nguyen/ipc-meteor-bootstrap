angular.module('rt600').directive('pmconfLog', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/reports/pm-configuration_log/pmconflog.html',
		controllerAs: 'pmconfLog',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
