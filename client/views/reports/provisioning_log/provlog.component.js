angular.module('rt600').directive('provLog', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/reports/provisioning_log/provlog.html',
		controllerAs: 'provLog',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
