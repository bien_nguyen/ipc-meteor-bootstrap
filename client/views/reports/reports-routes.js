angular.module('rt600').config(function ($stateProvider) {

	$stateProvider
		.state('alm-log', {
			template: '<alm-log></alm-log>'
		})
		.state('wc-conf-log', {
			template: '<wcconf-log></wcconf-log>'
		})
		.state('pm-conf-log', {
			template: '<pmconf-log></pmconf-log>'
		})
		.state('maint-log', {
			template: '<maint-log></maint-log>'
		})
		.state('prov-log', {
			template: '<prov-log></prov-log>'
		});
});
