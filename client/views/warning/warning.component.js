angular.module('rt600').directive('warning', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/warning/warning.html',
		controllerAs: 'warningctrl',
		controller: function($scope, $reactive, $rootScope, $state) {
			$reactive(this).attach($scope);
		}
	}
});
