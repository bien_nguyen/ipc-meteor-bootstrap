angular.module('rt600').directive('setresetPwd', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/user_management/set-reset_user_password/setresetpwd.html',
		controllerAs: 'setresetPwd',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
