angular.module('rt600').directive('setupUser', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/user_management/setup_users/setup-users.html',
		controllerAs: 'setupUser',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);

			this.helpers({
				allUsers: () => {
					return Meteor.users.find();
				}
			});

		}
	}
});
