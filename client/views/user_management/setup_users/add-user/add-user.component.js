angular.module('rt600').directive('addUser', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/user_management/setup_users/add-user/add-user.html',
		controllerAs: 'addUser',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);

			this.userobj = {};

			this.submit = () => {
				this.call('addUsr', this.userobj, (error, result) => {});
			};
		}
	}
});
