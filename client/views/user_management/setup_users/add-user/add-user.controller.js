angular.module('rt600').controller('AddUser', function($scope, $mdDialog) {
	'ngInject';

	$scope.userobj = {};

	$scope.dialogError = (msg) => {
		return $mdDialog.alert()
							.clickOutsideToClose(true)
							.title('Error')
							.textContent(msg)
							.ok('Close');
	}

	$scope.submit = () => {
		Meteor.call('addUsr', $scope.userobj, (error, result) => {
			console.log(error);
			if (error && error.error === "permission-denied") {
				$mdDialog.show(
					$scope.dialogError(error.reason)
				);
			} else if (error && error.error === "incorrect-arg") {
				$mdDialog.show(
					$scope.dialogError(error.reason)
				);
			} else if (error && error.error === "incorrect-property") {
				$mdDialog.show(
					$scope.dialogError(error.reason)
				);
			} else if (error && error.error === "user-exist") {
				$mdDialog.show(
					$scope.dialogError(error.reason)
				);
			} else if (error && error.error === "invalid-value") {
				$mdDialog.show(
					$scope.dialogError(error.reason)
				);
			} else if (error && error.error === "long-password") {
				$mdDialog.show(
					$scope.dialogError(error.reason)
				);
			} else if (error && error.error === "create-error") {
				$mdDialog.show(
					$scope.dialogError(error.reason)
				);
			} else {
				$mdDialog.show(
					$mdDialog.alert()
						.clickOutsideToClose(true)
						.title('Success')
						.textContent('User successfully added.')
						.ok('Close')
				);
			}
		});
	};
});
