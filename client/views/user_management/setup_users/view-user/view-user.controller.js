angular.module('rt600').controller('ViewUser', function($scope, $mdDialog) {
	'ngInject';

	$scope.generalToggle = false;
	$scope.techToggle = false;
	$scope.tech2Toggle = false;
	$scope.supervisorToggle = false;
	$scope.updateToggle = true;

	$scope.userobj = {};
	$scope.userobjsnapshot = {};

	$scope.updPermission = () => {
		var grp = GRPS.findOne({"users": Meteor.user().username})._id;
		if (ROLES.findOne({"_id":"setusr","grps":grp})) {
			return true;
		} else {
			return false;
		}
	};
	$scope.lockPermission = () => {
		if (GRPS.findOne({"users": Meteor.user().username})._id === "ADMIN") {
			return true;
		} else {
			return false;
		}
	};

	$scope.toggleGeneral = () => {
		$scope.generalToggle = $scope.generalToggle === true ? false: true;
	};
	$scope.toggleTech = () => {
		$scope.techToggle = $scope.techToggle === true ? false: true;
	};
	$scope.toggleTech2 = () => {
		$scope.tech2Toggle = $scope.tech2Toggle === true ? false: true;
	};
	$scope.toggleSupervisor = () => {
		$scope.supervisorToggle = $scope.supervisorToggle === true ? false: true;
	};
	$scope.toggleUpdate = () => {
		$scope.updateToggle = $scope.updateToggle === false ? true: false;
	};

	$scope.generalArrow = () => {
		if ($scope.generalToggle == true) {
			return "hardware:ic_keyboard_arrow_up_24px";
		} else if ($scope.generalToggle == false) {
			return "hardware:ic_keyboard_arrow_down_24px";
		}
	};
	$scope.techArrow = () => {
		if ($scope.techToggle == true) {
			return "hardware:ic_keyboard_arrow_up_24px";
		} else if($scope.techToggle == false) {
			return "hardware:ic_keyboard_arrow_down_24px";
		}
	};
	$scope.tech2Arrow = () => {
		if ($scope.tech2Toggle == true) {
			return "hardware:ic_keyboard_arrow_up_24px";
		} else if ($scope.tech2Toggle == false) {
			return "hardware:ic_keyboard_arrow_down_24px";
		}
	};
	$scope.supervisorArrow = () => {
		if ($scope.supervisorToggle == true) {
			return "hardware:ic_keyboard_arrow_up_24px";
		} else if ($scope.supervisorToggle == false) {
			return "hardware:ic_keyboard_arrow_down_24px";
		}
	};

	$scope.populate = (user) => {
		$scope.userobj.userid = user._id;
		$scope.userobj.eid = user.eid;
		$scope.userobj.username = user.username;
		$scope.userobj.fname = user.fname;
		$scope.userobj.lname = user.lname;
		$scope.userobj.mi = user.mi;
		$scope.userobj.grp = user.grp;
		$scope.userobj.address = user.addr;
		$scope.userobj.city = user.city;
		$scope.userobj.state = user.state;
		$scope.userobj.zip = user.zip;
		$scope.userobj.title = user.title;
		$scope.userobj.telephone = user.tel;
		$scope.userobj.email = user.email;
		$scope.userobj.stat = user.stat;
		$scope.userobj.supername = user.supv;
		$scope.userobj.comments = user.comment;
		$.extend($scope.userobjsnapshot,$scope.userobj);
		$scope.updateToggle = true;
	};
	$scope.lockUser = () => {
		Meteor.call('lockUsr', $scope.userobj.userid, (error, result) => {
			if (error && error.error === "status-lock") {
				$mdDialog.show(
					$mdDialog.alert()
						.clickOutsideToClose(true)
						.title('Error')
						.textContent('The user is already locked.')
						.ok('Close')
				);
			} else {
				$mdDialog.show(
					$mdDialog.alert()
						.clickOutsideToClose(true)
						.title('Success')
						.textContent('User successfully locked.')
						.ok('Close')
				);
			}
		});
	};
	$scope.unlockUser = () => {
		Meteor.call('unlockUsr', $scope.userobj.userid, (error, result) => {
			if (error && error.error === "status-lock") {
				$mdDialog.show(
					$mdDialog.alert()
						.clickOutsideToClose(true)
						.title('Error')
						.textContent('The user is not locked.')
						.ok('Close')
				);
			} else {
				$mdDialog.show(
					$mdDialog.alert()
						.clickOutsideToClose(true)
						.title('Success')
						.textContent('User successfully unlocked.')
						.ok('Close')
				);
			}
		});
	};
	$scope.cancel = () => {
		$.extend($scope.userobj,$scope.userobjsnapshot);
		$scope.updateToggle = true;
	};

	$scope.helpers({
		generalUsers: () => {
			return Meteor.users.find({"grp":"GENERAL"});
		},
		techUsers: () => {
			return Meteor.users.find({"grp":"TECH"});
		},
		tech2Users: () => {
			return Meteor.users.find({"grp":"TECH-II"});
		},
		supervisorUsers: () => {
			return Meteor.users.find({"grp":"SUPERVISOR"});
		}
	});
});
