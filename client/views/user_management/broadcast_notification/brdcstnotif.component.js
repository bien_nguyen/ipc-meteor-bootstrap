angular.module('rt600').directive('brdcstNotif', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/user_management/broadcast_notification/brdcstnotif.html',
		controllerAs: 'brdcstNotif',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);

			this.helpers({
				allUsers: () => {
					return Meteor.users.find();
				}
			});

    var
        nameList = ['Pierre', 'Pol', 'Jacques', 'Robert', 'Elisa'],
        familyName = ['Dupont', 'Germain', 'Delcourt', 'bjip', 'Menez'];

    function createRandomItem() {
        var
            firstName = nameList[Math.floor(Math.random() * 4)],
            lastName = familyName[Math.floor(Math.random() * 4)],
            age = Math.floor(Math.random() * 100),
            email = firstName + lastName + '@whatever.com',
            balance = Math.random() * 3000;

        return{
            firstName: firstName,
            lastName: lastName,
            age: age,
            email: email,
            balance: balance
        };
    }

		this.itemsByPage=15;

    this.rowCollection = [];
    for (var j = 0; j < 200; j++) {
        this.rowCollection.push(createRandomItem());
    }

		}
	}
});
