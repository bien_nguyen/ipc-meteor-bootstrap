angular.module('rt600').directive('userSrch', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/user_management/user_search/usersrch.html',
		controllerAs: 'userSrch',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
