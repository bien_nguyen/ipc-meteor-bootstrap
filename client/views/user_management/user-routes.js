angular.module('rt600').config(function ($urlRouterProvider, $stateProvider) {
	$stateProvider
		.state('setup-user', {
			template: '<setup-user></setup-user>'
		})
		.state('brdcst-notif', {
			template: '<brdcst-notif></brdcst-notif>'
		})
		.state('user-search', {
			template: '<user-srch></user-srch>'
		})
		.state('setreset-pwd', {
			template: '<setreset-pwd></setreset-pwd>'
		});
});
