angular.module('rt600', [
	'angular-meteor',
	'ui.router',
	'accounts.ui',
	'smart-table'
]);

Accounts.config({
	forbidClientAccountCreation: true
});

Accounts.ui.config({
	passwordSignupFields: 'USERNAME_ONLY',
});

function onReady() {
	angular.bootstrap(document, ['rt600'], {
		strictDi: true
	});
}

if (Meteor.isCordova)
	angular.element(document).on("deviceready", onReady);
else
	angular.element(document).ready(onReady);
